# autofittextview

## Introduction
> autofittextview implements a text view that automatically resizes text to fit perfectly within its boundaries.

## Effect
![sample.gif](sample.gif)

## How to Install
```shell
ohpm install @ohos/autofittextview
```
For details about the OpenHarmony ohpm environment configuration, see [OpenHarmony HAR](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use

### Initialization
```
 @State content: string= ''
    @State textSize: number= 20
    @State maxLines: number= 2
    @State width: string= '100%'
    @State model: AutofitTextView.Model= new AutofitTextView.Model()
```

### Setting Attributes
```
    private aboutToAppear() {
      this.model.setWidth(this.width)
      this.model.setText(this.content)
      this.model.setMaxLines(2)
      this.model.setTextSize(10,TypedValue.COMPLEX_UNIT_PX)
      this.model.setMinTextSize(2)
      this.model.setMaxTextSize(40)
   }
```
### Using AutofitTextView in build()
```
    build() {
      Column() {
        AutofitTextView({ model: this.model });
      }
      .alignItems(HorizontalAlign.Start)
      .padding(10)
      .width('100%')
      .height('100%')
    }
```

## Available APIs
`@State model: AutofitTextView.Model= new AutofitTextView.Model()`
1. Sets the component width.
   `model.setWidth('100%')`
2. Sets the text size.
   `model.setTextSize(10,TypedValue.COMPLEX_UNIT_PX)`
3. Sets the maximum text size.
   `model.setMaxTextSize(40)`
4. Sets the minimum text size.
   `model.setMinTextSize(2)`
5. Sets the maximum number of displayed lines.
   `model.setMaxLines(2)`
6. Sets the text content.
   `model.setText('test')`
7. Sets the background color.
   `model.setBackgroundColor(0XD1C9C9)`
8. Sets the scrollbar size.
   `model.setTextOverflow(32)`
9. Sets whether to enable adaptive sizing.
   `model.isSizeToFit()`
10. Enables adaptive sizing.
       `model.setSizeToFit(true)`
11. Obtains the maximum text size.
       `model.getMaxTextSize()`
12. Obtains the minimum text size.
       `model.getMinTextSize()`
13. Sets the precision value of the correct text size.
       `model.setPrecision(1)`
14. Obtains the precision value of the correct text size.
       `model.getPrecision()`

## Constraints

This project has been verified in the following version:

- DevEco Studio NEXT Developer Beta3: (5.0.3.530), SDK: API 12 (5.0.0.35 (SP3))
## Directory Structure
````
|---- ohos-autofittextview
|     |---- entry  # Sample code
|     |---- Autofittextview  # autofittextview library
                   |---- scr/main/ets/Autofittextview   # Custom text component
                   |---- scr/main/ets/TypedValue   # Enumeration class of the text size unit
|           |---- index.ets  # External APIs
|     |---- README.md  # Readme
````

## How to Contribute
If you find any problem when using the project, submit an [issue](https://gitee.com/openharmony-sig/ohos-autofittextview/issues) or a [PR](https://gitee.com/openharmony-sig/ohos-autofittextview/pulls).

## License
This project is licensed under [Apache License 2.0](https://gitee.com/openharmony-sig/ohos-autofittextview/blob/master/LICENSE).
